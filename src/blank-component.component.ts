import { Component } from '@angular/core';

@Component({
  selector: 'blank-component',
  templateUrl: 'blank-component.component.html',
  styleUrls: ['blank-component.component.scss'],
})
export class BlankComponent {

  constructor() {}

}
