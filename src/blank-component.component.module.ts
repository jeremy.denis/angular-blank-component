import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlankComponent } from './blank-component.component';

@NgModule({
  declarations: [ BlankComponent ],
  exports: [BlankComponent],
  imports: [
    CommonModule
  ]
}) 
export class BlankComponentModule {}
